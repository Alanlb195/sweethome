package com.example.test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    CardView cdPlantaBaja;
    CardView cdPlantaAlta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cdPlantaBaja = findViewById(R.id.cdPlantaBaja);
        cdPlantaAlta = findViewById(R.id.cdPlataAlta);
    } // End onCreate

    public void plantaBaja(View view) {
        Intent i = new Intent(this, firstFloor.class);
        startActivity(i);
    }

    public void plantaAlta(View view) {
        Intent i = new Intent(this, secondFloor.class);
        startActivity(i);
    }



} // End MainActivity
